<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FetchLocation extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This comment use to fetch provinces and cities to save in our Database';

    private $client;

    /**
     * Create a new command instance.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @param Client $client
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $response = $this->client->request(
            'GET', 'https://api.rajaongkir.com/starter/province',
            array(
                'headers' => array('key' => '0df6d5bf733214af6c6644eb8717c92c')
            )
        )->getBody();

        $response_array = json_decode($response, true);

        for ($i = 0; $i < count($response_array['rajaongkir']['results']); $i++) {

            DB::table('province')->insert([
                "id" => $response_array['rajaongkir']['results'][$i]['province_id'],
                "province" => $response_array['rajaongkir']['results'][$i]['province']
            ]);
        }

        $response_city = $this->client->request(
            'GET', 'https://api.rajaongkir.com/starter/city',
            array(
                'headers' => array('key' => '0df6d5bf733214af6c6644eb8717c92c')
            )
        )->getBody();

        $response_array_city = json_decode($response_city, true);

        for ($i = 0; $i < count($response_array_city['rajaongkir']['results']); $i++) {

            DB::table('city')->insert([
                "id" => $response_array_city['rajaongkir']['results'][$i]['city_id'],
                "province_id" => $response_array_city['rajaongkir']['results'][$i]['province_id'],
                "city" => $response_array_city['rajaongkir']['results'][$i]['city_name']
            ]);
        }

    }
}
