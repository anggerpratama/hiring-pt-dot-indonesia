<?php

namespace App\Http\Controllers;

use App\City;
use App\Province;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function getProvince(Request $request)
    {

        $result = Province::query()->find($request->id);

        return response()->json($result, 200);

    }

    public function getCity(Request $request)
    {

        $result = City::query()->find($request->id);

        return response()->json($result, 200);

    }
}
