<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')
            ->insert([
                "name" => "Angger",
                "email" => "angger@mail.com",
                "password" => \Illuminate\Support\Facades\Hash::make('123123123'),
                "created_at" => \Carbon\Carbon::now()
            ]);
    }
}
