<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        try {

            $checkUser = User::query()->where('email', '=', $request->email)->firstOrFail();

            if (Hash::check($request->password, $checkUser->password)) {
                $token = str_random(10);

                User::query()->where('email', '=', $request->email)->update([
                    "token" => $token
                ]);

                return response()->json([
                    "token" => $token
                ]);
            }

        } catch (\Exception $exception) {

            if ($exception instanceof ModelNotFoundException) {

                return response()->json('error login');

            }

        }


    }
}
